"""# 8 node directed graph example from Shaja and Sudhakar.

Reference:
    Shaja, A. S., & Sudhakar, K. (2010). Optimized sequencing of analysis components in
    multidisciplinary systems. Research in Engineering Design, 21(3), 173-187.
    [DOI: 10.1007/s00163-009-0082-5](https://doi.org/10.1007/s00163-009-0082-5)
"""

edge_weights = ["adjacency"]
