"""Graph of a design problem complexity study. Components are related (have edges) to
attributes that are of importance, which are of edge kind "incidence". Nodes have kind
"component" or "attribute".

Reference:
    [Figure 1(a)] Chen, L., & Li, S. (2005). Analysis of Decomposability and
    Complexity for Design Problems in the Context of Decomposition. Journal of Mechanical
    Design, 127(4), 545. [DOI: 10.1115/1.1897405](https://doi.org/10.1115/1.1897405)
"""
