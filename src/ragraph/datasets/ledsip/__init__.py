"""# LED system-in-package (LEDSiP, 1200 components)

Reference:
Wilschut, T. (2014). Analysis of the multi-disciplinary coupling structure of a LED
system-in-package.
"""

edge_weights = ["adjacency"]
