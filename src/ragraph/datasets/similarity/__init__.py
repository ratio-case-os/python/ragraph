"""# Similarity analysis example

Example graph for similarity analysis. Products are related (have edges) to attributes that are of
importance in product portfolio analysis. Edges have edge kind "incidence". Nodes have kind
"product" or "attribute".
"""
