"""RaGraph

Ratio graph handling in Python.
"""

__version__ = "1.22.7"

from logging import Logger, getLogger

logger: Logger = getLogger(__name__)
"""RaGraph's central logger instance."""
